# CS客服系统

#### 介绍

CS是基于 Golang开发的简单客服工单系统。集成了消息实时收发, 工单指派, 多用户权限管理等功能

#### 技术栈
Golang + gin + vue3 + Element Puls

#### 项目运行

```
git clone https://gitee.com/null_488_0272/cs.git

cd cs

go run main.go
```


#### 安装教程

1.  执行database文件夹下的sql文件同步数据库
2.  在项目根目录执行go mod tidy 命令安装项目依赖包
3.  在项目根目录执行go run main.go 命令运行项目

#### 使用说明

1.  访问客户端http://127.0.0.1:9080/
2.  访问服务端http://127.0.0.1:9080/home
3.  服务端登录账号 admin@qq.com  124816

#### 项目预览客户端
![输入图片说明](https://images.gitee.com/uploads/images/2021/1118/111813_46ed1461_1070101.png "B3A8DF42-4580-43d2-844E-81DE06720F0D.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1118/111835_ae3518ee_1070101.png "F3FFF554-543D-42c5-AFA4-30CF89F0250F.png")

#### 项目预览服务端
![输入图片说明](https://images.gitee.com/uploads/images/2021/1118/111911_572ec5fb_1070101.png "9018876D-8D7D-4f0c-A3BB-AA608EAA87BB.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1118/111928_29a2997b_1070101.png "FE29FB15-B982-4cfb-AA7B-80141D91438B.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1118/111941_09389de1_1070101.png "07C28161-D810-4640-A44F-71D5BD7D340F.png")