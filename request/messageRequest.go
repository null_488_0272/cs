package request

type AddMessageForm struct {
	Content        string `form:"content" json:"content" binding:"required"`
	Type           int    `form:"type" json:"type" binding:"required"`
	Sender         int    `form:"sender" json:"sender" binding:"required"`
	Receiver       int    `form:"receiver" json:"receiver" binding:"required"`
	From           int    `form:"from" json:"from" binding:"required"`
	ToRole         string `form:"to_role" json:"to_role" binding:"required"`
	ConversationId int    `form:"conversation_id" json:"conversation_id" binding:"required"`
}
