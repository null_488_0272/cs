package request

type AddTipsForm struct {
	Name    string `form:"name" json:"name" binding:"required"`
	Content string `form:"Content" json:"Content" binding:"required"`
}
