package request

type AddConversationForm struct {
	UserId  int `form:"user_id" json:"user_id" binding:"required"`
	GuestId int `form:"guest_id" json:"guest_id" binding:"required"`
}

type AssignConversationForm struct {
	UserId         int `form:"user_id" json:"user_id" binding:"required"`
	ConversationId int `form:"conversation_id" json:"conversation_id" binding:"required"`
}

type AssignAllConversationForm struct {
	FromUid int `form:"from_uid" json:"from_uid" binding:"required"`
	ToUid   int `form:"to_uid" json:"to_uid" binding:"required"`
}

type SetNoteForm struct {
	Note           string `form:"note" json:"note" binding:"required"`
	ConversationId int    `form:"conversation_id" json:"conversation_id" binding:"required"`
}
