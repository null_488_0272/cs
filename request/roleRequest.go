package request

type AddRoleForm struct {
	Name    string `form:"name" json:"name" binding:"required"`
	MenuIds []int  `form:"menuIds" json:"menuIds" binding:"required"`
}

type UpdateRoleForm struct {
	ID      int    `form:"id" json:"id" binding:"required"`
	Name    string `form:"name" json:"name" binding:"required"`
	MenuIds []int  `form:"menuIds" json:"menuIds" binding:"required"`
}
