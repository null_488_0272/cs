package request

type AddMenuForm struct {
	Name     string `form:"id" json:"name" binding:"required"`
	Sort     int    `form:"name" json:"sort" binding:"required"`
	Route    string `form:"route" json:"route"`
	Icon     string `form:"icon" json:"icon"`
	ParentId int    `form:"parent_id" json:"parent_id"`
}

type UpdateMenuForm struct {
	ID       int    `form:"id" json:"id" binding:"required"`
	Name     string `form:"name" json:"name" binding:"required"`
	Sort     int    `form:"sort" json:"sort" binding:"required"`
	Route    string `form:"route" json:"route"`
	Icon     string `form:"icon" json:"icon"`
	ParentId int    `form:"parent_id" json:"parent_id"`
}
