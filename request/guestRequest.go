package request

type AddGuestForm struct {
	Name string `form:"name" json:"name" binding:"required"`
	Uid  string `form:"uid" json:"Uid" binding:"required"`
}
