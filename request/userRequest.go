package request

type LoginForm struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type AddUserForm struct {
	Name     string `form:"name" json:"name" binding:"required"`
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
	RoleId   int    `form:"role_id" json:"role_id" binding:"required"`
}

type UpdateUserForm struct {
	ID     int    `form:"id" json:"id" binding:"required"`
	Name   string `form:"name" json:"name" binding:"required"`
	Email  string `form:"email" json:"email" binding:"required"`
	RoleId int    `form:"role_id" json:"role_id" binding:"required"`
}

type ResetPasswordForm struct {
	ID       int    `form:"id" json:"id" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}
