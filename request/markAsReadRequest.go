package request

type MarkAsReadForm struct {
	ConversationId int    `form:"conversation_id" json:"conversation_id" binding:"required"`
	Owner          string `form:"owner" json:"owner" binding:"required"`
}
