package routes

import (
	"cs/api"
	"cs/middleware"
	"cs/ws"

	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
)

func createMyRender() multitemplate.Renderer {
	p := multitemplate.NewRenderer()
	p.AddFromFiles("backend", "web/dist/admin.html")
	p.AddFromFiles("frontend", "web/dist/index.html")
	return p
}

func InitRouter(router *gin.Engine) {
	router.Use(middleware.Cors())

	router.HTMLRender = createMyRender()

	router.Static("/upload", "./upload")
	router.Static("/public", "./web/dist")
	router.Static("/assets", "./web/dist/assets")
	router.StaticFile("/favicon.ico", "./web/dist/favicon.ico")

	router.GET("/home", func(c *gin.Context) {
		c.HTML(200, "backend", nil)
	})
	router.GET("/", func(c *gin.Context) {
		c.HTML(200, "frontend", nil)
	})

	//前后聊天
	router.GET("/ws/user/:uid", func(c *gin.Context) {
		ws.ServeWs(c, "user")
	})
	router.GET("/ws/guest/:uid", func(c *gin.Context) {
		ws.ServeWs(c, "guest")
	})

	frontend := router.Group("/api")
	{
		frontend.GET("/getConversationInfo", api.GetConversationInfo)
		frontend.PUT("/message/markAsRead", api.MarkAsRead)
		frontend.POST("/message/add", api.AddMessage)
		frontend.POST("/uploadImage", api.UploadImage)
		frontend.POST("/guest/login", api.GuestLogin)
	}

	backend := router.Group("/admin")
	{
		backend.POST("/signin", api.Login)

		backend.Use(middleware.JWT())
		{
			backend.GET("/menus", api.GetMenus)
			backend.GET("/getMenuTree", api.GetMenuTree)
			backend.GET("/getTopMenu", api.GetTopMenu)
			backend.GET("/getSubmenu", api.GetSubmenu)
			backend.GET("/getMyselfMenu", api.GetMyselfMenu)
			backend.PUT("/menu/:id", api.UpdateMenu)
			backend.DELETE("/menu/:id", api.DelMenu)
			backend.POST("/menu/add", api.AddMenu)

			backend.GET("/roles", api.GetRoles)
			backend.PUT("role/:id", api.UpdateRole)
			backend.DELETE("role/:id", api.DeleteRole)
			backend.POST("/role/add", api.AddRole)

			backend.GET("/users", api.GetUsers)
			backend.GET("/user/all", api.GetAllUser)
			backend.POST("/user/add", api.AddUser)
			backend.PUT("user/:id", api.UpdateUser)
			backend.DELETE("user/:id", api.DeleteUser)
			backend.PUT("/user/resetPassword/:id", api.ResetPassword)

			backend.GET("/messages", api.GetMessages)
			backend.DELETE("message/:id", api.DeleteMessage)

			backend.GET("/conversation/pending", api.GetPendingConversation)
			backend.GET("/conversation/processing", api.GetProcessingConversation)
			backend.GET("/conversation/find", api.FindConversation)
			backend.POST("/conversation/add", api.AddConversation)
			backend.PUT("/conversation/assign", api.AssignConversation)
			backend.PUT("/conversation/assignAll", api.AssignAllConversation)
			backend.PUT("/conversation/setNote", api.SetNote)
			backend.PUT("/conversation/:conversation_id/close", api.CloseConversation)
			backend.PUT("/conversation/:conversation_id/active", api.ActiveConversation)
			backend.PUT("/conversation/:conversation_id/reopen", api.ReopenConversation)

			backend.GET("/tips/get/:user_id", api.GetTips)
			backend.POST("/tips/add", api.AddTips)
			backend.DELETE("/tips/:id", api.DeleteTips)

			backend.PUT("/guest/markAsVip/:guest_id", api.MarkAsVip)
			backend.GET("/guest/isOnline/:guest_id", api.IsOnline)
		}
	}
}
