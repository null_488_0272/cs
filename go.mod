module cs

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/gzip v0.0.3
	github.com/gin-contrib/multitemplate v0.0.0-20211002122701-e9e3201b87a0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-basic/uuid v1.0.0
	github.com/gorilla/websocket v1.4.2
	github.com/lionsoul2014/ip2region v2.2.0-release+incompatible
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.22.0
)
