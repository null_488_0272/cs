package main

import (
	"cs/models"
	"cs/routes"
	"cs/ws"

	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
)

func main() {
	go ws.Run()

	//初始化数据库连接
	models.InitDb()

	router := gin.Default()
	router.Use(gzip.Gzip(gzip.DefaultCompression))
	// 设置路由信息
	routes.InitRouter(router)

	router.Run(":9080")
}
