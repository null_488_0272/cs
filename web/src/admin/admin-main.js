import { createApp } from 'vue'
import router from './router'
import App from './Admin.vue'
import './assets/index.css'
import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css';

const app = createApp(App)

app.use(router)
app.use(ElementPlus)
app.mount('#app')

