import http from '../../utils/http'

export function getTips(uid) {
  return http.get(`/admin/tips/get/${uid}`)
}

export function addTips(data) {
  return http.post('/admin/tips/add', data)
}

export function delTips(id) {
  return http.delete(`/admin/tips/${id}`)
}
