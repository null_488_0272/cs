import http from '../../utils/http'

export function markAsVip(guest_id) {
  return http.put(`/admin/guest/markAsVip/${guest_id}`)
}

export function guestIsOnline(guest_id) {
  return http.get(`/admin/guest/isOnline/${guest_id}`)
}