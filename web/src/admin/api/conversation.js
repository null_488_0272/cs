import http from '../../utils/http'

export function getConversations(type) {
  if (type === 'PENDING') {
    return http.get('/admin/conversation/pending')
  } else {
    return http.get('/admin/conversation/processing')
  }
}

export function assignConversation(conversation_id, user_id) {
  return http.put(`/admin/conversation/assign`, { user_id, conversation_id })
}

export function assignAllConversation(from_uid, to_uid) {
  return http.put(`/admin/conversation/assignAll`, { from_uid, to_uid })
}


export function searchConversation(keyword) {
  return http.get(`/admin/conversation/find`, { params: { keyword } })
}

export function setConversationNote(conversation_id, note) {
  return http.put(`/admin/conversation/setNote`, { note, conversation_id })
}

export function closeConversation(conversation_id) {
  return http.put(`/admin/conversation/${conversation_id}/close`)
}

export function activeConversation(conversation_id) {
  return http.put(`/admin/conversation/${conversation_id}/active`)
}

export function reopenConversation(conversation_id) {
  return http.put(`/admin/conversation/${conversation_id}/reopen`)
}
