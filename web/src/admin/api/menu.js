import http from '../../utils/http'

export function getMenus() {
    return http.get('/admin/menus')
}

export function getMenuTree() {
    return http.get('/admin/getMenuTree')
}

export function getTopMenu() {
    return http.get('/admin/getTopMenu')
}

export function getSubmenu() {
    return http.get('/admin/getSubmenu')
}

export function getMyselfMenu() {
    return http.get('/admin/getMyselfMenu')
}

export function addMenu(data) {
    return http.post('/admin/menu/add', data)
}

export function updateMenu(data) {
    return http.put(`/admin/menu/${data.id}`, data)
}

export function delMenu(id) {
    return http.delete(`/admin/menu/${id}`)
}