import http from '../../utils/http'

export function getRoles() {
  return http.get('/admin/roles')
}

export function addRole(data) {
  return http.post('/admin/role/add', data)
}

export function updateRole(data) {
  return http.put(`/admin/role/${data.id}`, data)
}

export function delRole(id) {
  return http.delete(`/admin/role/${id}`)
}
