import http from '../../utils/http'

export function getMessages(conversation_id, page_num, page_size) {
  return http.get('/admin/messages', {params: {conversation_id, page_num, page_size}})
}

export function addMessage(data) {
  return http.post('/api/message/add', data)
}

export function delMessage(id) {
  return http.delete(`/admin/message/${id}`)
}

export function markMsgAsRead(conversation_id) {
  return http.put('/api/message/markAsRead', {conversation_id, owner: "user"})
}

export function uploadImage(form_data) {
  return http.post('/api/uploadImage', form_data)
}