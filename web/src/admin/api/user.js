import http from '../../utils/http'

export function getUsers(keyword, pageNum, pageSize) {
    return http.get('/admin/users', { params: { keyword, pageSize, pageNum } })
}

export function getAllUser() {
    return http.get('/admin/user/all')
}

export function addUser(data) {
    return http.post('/admin/user/add', data)
}

export function updateUser(data) {
    return http.put(`/admin/user/${data.id}`, data)
}

export function delUser(id) {
    return http.delete(`/admin/user/${id}`)
}

export function resetPassword(id, password) {
    return http.post('/admin/user/resetPassword', { id, password })
}

export function login(email, password) {
    return http.post('/admin/signin', { email, password })
}
