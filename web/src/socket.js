let socket

const onerror = function (e) {
  console.log('websocket 服务连接失败.....')
}

const onclose = function (e) {
  console.log('websocket 服务连接关闭.....')
}

const onopen = function (e) {
  console.log('websocket 服务连接打开.....')
}

const ws = {
  init(wsUrl) {
    socket = new WebSocket(wsUrl)
    socket.onclose = onclose;
    socket.onerror = onerror;
    socket.onopen = onopen
  },
  emit(data) {
    let message = JSON.stringify(data);
    socket.send(message)
  },
  onmessage(fun) {
    socket.onmessage = fun;
  }
}

export default ws
