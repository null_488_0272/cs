export default {
  apiUrl: '//127.0.0.1:9080',
  // apiUrl: '//192.168.2.35:9080',
  development: {
    baseUrl: '//127.0.0.1:9080' // 测试接口域名
  },
  beta: {
    baseUrl: '//127.0.0.1:9080' // 测试接口域名
  },
  release: {
    baseUrl: '//127.0.0.1:9080' // 正式接口域名
  },
  wsUrl: 'ws://localhost:9080/ws'
}