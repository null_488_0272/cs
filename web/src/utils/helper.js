//判断是不是苹果设备
export function isIos() {
  let u = navigator.userAgent;
  return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
}

//判断时间戳是否为当天时间
export function isToday(str) {
  return new Date(str).toDateString() === new Date().toDateString() ? true : false
}

export function simplifyDate(date) {
  return isToday(date)
      ? dateFormat(date, 'HH:mm')
      : dateFormat(date, 'MM-dd')
}

export function fmtLatestMsg(data) {
  if (!data || !data.content) {
    return '你撤回了一条消息'
  } else if (data.type == 1) {
    return data.content.replace(/\[em_([0-9]*)\]/g, '<img src="/public/face/$1.gif" style="height: 17px"/>')
  } else if (data.type == 3) {
    return `[图片]`
  }
}

//将base64图片资源转换成Blob格式
export function base64ToBlob(base64) {
  let bytes = window.atob(base64.split(',')[1])
      , size = bytes.length
      , u8arr = new Uint8Array(size);
  while (size--) {
    u8arr[size] = bytes.charCodeAt(size);
  }
  return new Blob([u8arr], {type: 'image/png'});
}

//将时间戳转化为日期格式
export function timestampToDate(timestamp = '') {
  timestamp = timestamp || +new Date();
  let date = new Date(timestamp + 8 * 3600 * 1000);

  return date.toJSON().substr(0, 19).replace('T', ' ');
}

export function isMobile() {
  const userAgentInfo = navigator.userAgent
  let mobileAgents = [
    'Android',
    'iPhone',
    'SymbianOS',
    'Windows Phone',
    'iPad',
    'iPod',
  ]
  return mobileAgents.some(i => userAgentInfo.includes(i))
}

//格式化日期
export function dateFormat(date, fmt) {
  date = date instanceof Date ? date : new Date(date)

  let o = {
    'M+': date.getMonth() + 1, // 月份
    'd+': date.getDate(), // 日
    'h+': date.getHours() % 12 === 0 ? 12 : date.getHours() % 12, // 小时
    'H+': date.getHours(), // 小时
    'm+': date.getMinutes(), // 分
    's+': date.getSeconds(), // 秒
    'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
    'S': date.getMilliseconds() // 毫秒
  };
  let week = {
    '0': '/u65e5',
    '1': '/u4e00',
    '2': '/u4e8c',
    '3': '/u4e09',
    '4': '/u56db',
    '5': '/u4e94',
    '6': '/u516d'
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  if (/(E+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? '/u661f/u671f' : '/u5468') : '') + week[date.getDay() + '']);
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
    }
  }
  return fmt;
}
