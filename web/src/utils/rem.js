// 设置基准大小
const baseSize = 32
function setRem () {
  // 当前页面宽度相对于 750 宽的缩放比例
  const UIwidth = 750, rem2px = 100;
  const scale = document.documentElement.clientWidth / UIwidth
  document.documentElement.style.fontSize = (rem2px * Math.min(scale, 2)) + 'px'
  console.log(document.documentElement.style.fontSize)
}
// 初始化
setRem()
window.onresize = function () {
  setRem()
}
