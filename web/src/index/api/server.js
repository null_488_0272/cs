import http from '../../utils/http'

export function getConversationInfo(guest_id) {
  return http.get('/api/getConversationInfo', { params: { guest_id } })
}


export function addMessage(data) {
  return http.post('/api/message/add', data)
}

export function markAsRead(conversation_id) {
  return http.put('/api/message/markAsRead', { conversation_id, owner: "guest" })
}

export function uploadImage(form_data) {
  return http.post('/api/uploadImage', form_data)
}

