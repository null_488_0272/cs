import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  hostname: '0.0.0.0', // 默认是 localhost
  port: '8000', // 默认是 3000 端口
  https: false, // 是否开启 https
  ssr: false, // 服务端渲染
  base: process.env.NODE_ENV === "production" ? "./" : "", // 生产环境下的公共路径
  proxy: { // 本地开发环境通过代理实现跨域，生产环境使用 nginx 转发
    '/api': {
      target: 'http://127.0.0.1:7001', // 后端服务实际地址
      changeOrigin: true,
      rewrite: path => path.replace(/^\/api/, '')
    }
  },
  plugins: [vue()],
  server: {
    host: '0.0.0.0'
  },
  build: {
    rollupOptions: {
      input: {
        index: path.resolve(__dirname, 'index.html'),
        admin: path.resolve(__dirname, 'admin.html'),
      },
      output: {
        //解决打包时Some chunks are larger警告
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return id.toString().split('node_modules/')[1].split('/')[0].toString();
          }
        }
      }
    },
  }
})
