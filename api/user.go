package api

import (
	"cs/models"
	"cs/request"
	"cs/util"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// Login 后台登陆
func Login(c *gin.Context) {
	var form request.LoginForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}

	user, err := models.CheckUserAuth(form.Email, form.Password)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": "账号或密码错误", "extra": err})
		return
	}

	token, err := util.GenerateToken(user.ID, user.Email, user.Password)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": "token生成失败", "extra": err})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":   "success",
		"message":  "ok",
		"token":    token,
		"username": user.Name,
		"user_id":  user.ID,
	})
}

func GetAllUser(c *gin.Context) {
	users, err := models.GetAllUser()
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "data": users})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func GetUsers(c *gin.Context) {
	keyword := c.Query("keyword")
	pageSize, _ := strconv.Atoi(c.Query("pageSize"))
	pageNum, _ := strconv.Atoi(c.Query("pageNum"))

	users, total, _ := models.GetUsers(keyword, pageNum, pageSize)

	c.JSON(http.StatusOK, gin.H{"status": "success", "users": users, "total": total})
}

func AddUser(c *gin.Context) {
	var form request.AddUserForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}
	//对密码进行加密
	pwdHash, _ := bcrypt.GenerateFromPassword([]byte(form.Password), 5)

	user := models.User{
		Name:     form.Name,
		RoleId:   form.RoleId,
		Email:    form.Email,
		Password: string(pwdHash),
	}

	if err := models.AddUser(user); err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "message": "ok"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func UpdateUser(c *gin.Context) {
	var form request.UpdateUserForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}

	err := models.UpdateUser(form.ID, map[string]interface{}{
		"name":    form.Name,
		"email":   form.Email,
		"role_id": form.RoleId,
	})

	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "message": "ok"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func ResetPassword(c *gin.Context) {
	var form request.ResetPasswordForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}

	pwdHash, _ := bcrypt.GenerateFromPassword([]byte(form.Password), 5)
	if err := models.ResetPwd(form.ID, pwdHash); err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "message": "ok"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func DeleteUser(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	if err := models.DelUser(id); err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "message": "ok"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}
