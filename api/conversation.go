package api

import (
	"cs/models"
	"cs/request"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetPendingConversation(c *gin.Context) {
	user_id, _ := c.Get("user_id")
	data, _ := models.GetConversationsByUserId(user_id, "PENDING")

	c.JSON(http.StatusOK, gin.H{"message": "success", "data": data})
}

func GetProcessingConversation(c *gin.Context) {
	user_id, _ := c.Get("user_id")
	data, _ := models.GetConversationsByUserId(user_id, "PROCESSING")

	c.JSON(http.StatusOK, gin.H{"message": "success", "data": data})
}

func AddConversation(c *gin.Context) {
	var form request.AddConversationForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}

	conversation := models.Conversation{
		UserId:  form.UserId,
		GuestId: form.GuestId,
	}
	if err := models.AddConversation(conversation); err != nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "data": conversation})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func AssignConversation(c *gin.Context) {
	var form request.AssignConversationForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}
	err := models.AssignConversation(form.ConversationId, form.UserId)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func AssignAllConversation(c *gin.Context) {
	var form request.AssignAllConversationForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}
	err := models.AssignAllConversation(form.FromUid, form.ToUid)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func FindConversation(c *gin.Context) {
	keyword := c.Query("keyword")
	if len(keyword) == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "缺少参数"})
		return
	}
	data, err := models.SearchConversation(keyword)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "data": data})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func SetNote(c *gin.Context) {
	var form request.SetNoteForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}
	err := models.SetConversationNote(form.ConversationId, form.Note)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func ActiveConversation(c *gin.Context) {
	user_id, _ := c.Get("user_id")
	conversation_id := c.Param("conversation_id")
	err := models.ActiveConversation(conversation_id, user_id)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func CloseConversation(c *gin.Context) {
	conversation_id := c.Param("conversation_id")
	err := models.CloseConversation(conversation_id)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func ReopenConversation(c *gin.Context) {
	conversation_id := c.Param("conversation_id")
	err := models.ReopenConversation(conversation_id)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}
