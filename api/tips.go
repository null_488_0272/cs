package api

import (
	"cs/models"
	"cs/request"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetTips(c *gin.Context) {
	uid, _ := strconv.Atoi(c.Param("user_id"))
	tipsList, err := models.GetTipsByUid(uid)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "tips_list": tipsList})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func AddTips(c *gin.Context) {
	var form request.AddTipsForm
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "参数验证失败"})
		return
	}
	user_id, _ := c.Get("user_id")
	tips := models.Tips{
		UserId:  user_id.(int),
		Name:    form.Name,
		Content: form.Content,
	}
	if err := models.AddTips(tips); err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "data": tips})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}

func DeleteTips(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	if err := models.DelTips(id); err == nil {
		c.JSON(http.StatusOK, gin.H{"status": "success", "message": "ok"})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "failed", "message": err.Error()})
	}
}
