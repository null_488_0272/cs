package util

import (
	"fmt"

	"github.com/lionsoul2014/ip2region/binding/golang/ip2region"
)

func ParseIp(ip string) string {
	region, err := ip2region.New("ip2region.db")
	defer region.Close()
	if err != nil {
		fmt.Println(err)
		return "--"
	}
	ipInfo, _ := region.BtreeSearch(ip)
	fmt.Println(ip, err)

	zone := fmt.Sprintf("%s%s%s", ipInfo.Country, ipInfo.Region, ipInfo.City)

	return zone
}
