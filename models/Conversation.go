package models

import (
	"time"
)

type Conversation struct {
	ID          int       `json:"id"`
	UserId      int       `json:"user_id"`
	GuestId     int       `json:"guest_id"`
	Status      string    `json:"status"`
	Note        string    `json:"note"`
	UserUnread  int       `json:"user_unread"`
	GuestUnread int       `json:"guest_unread"`
	LateMsgAt   time.Time `json:"late_msg_at"`
	CreatedAt   time.Time `json:"created_at"`
	Guest       Guest     `json:"guest"`
	LateMsg     Message   `json:"late_msg"`
	Messages    []Message `json:"messages"`
}

func AddConversation(conversation Conversation) (err error) {
	err = db.Create(&conversation).Error

	return
}

func GetConversationsByUserId(user_id interface{}, status string) (conversations []Conversation, err error) {
	err = db.Preload("Guest").
		Preload("LateMsg", "is_late =?", 1).
		Where("user_id =?", user_id).
		Where("status =?", status).
		Order("late_msg_at desc").
		Find(&conversations).
		Error

	return
}

func GetConversationInfo(guest_id interface{}) (conversation Conversation, err error) {
	err = db.Preload("Messages").Where("guest_id =?", guest_id).First(&conversation).Error

	return
}

//更换会话的处理人
func AssignConversation(conversation_id int, user_id int) error {
	result := db.Model(&Conversation{}).Where("id = ?", conversation_id).Update("user_id", user_id)

	return result.Error
}

//更换会话的处理人
func AssignAllConversation(from_uid int, to_uid int) error {
	result := db.Model(&Conversation{}).Where("user_id = ?", from_uid).Update("user_id", to_uid)

	return result.Error
}

//对会话进行搜索
func SearchConversation(keyword string) (conversations []Conversation, err error) {
	var guestIds []int64
	match := "%" + keyword + "%"
	err = db.Model(&Guest{}).Where("name like ?", match).Or("uid like ?", match).Pluck("id", &guestIds).Error
	if err != nil {
		return
	}

	if len(guestIds) > 0 {
		err = db.Preload("Guest").
			Preload("LateMsg", "is_late =?", 1).
			Where("status <> ?", "PROCESSED").
			Where("guest_id IN ?", guestIds).
			Find(&conversations).
			Error
	}

	return
}

//设置备注
func SetConversationNote(conversation_id int, note string) (err error) {
	err = db.Model(&Conversation{}).Where("id = ?", conversation_id).Update("note", note).Error

	return
}

//关闭会话
func CloseConversation(conversation_id interface{}) (err error) {
	err = db.Model(&Conversation{}).Where("id = ?", conversation_id).Update("status", "PROCESSED").Error

	return
}

//将会话设置为处理中的状态
func ReopenConversation(conversation_id interface{}) (err error) {
	err = db.Model(&Conversation{}).Where("id = ?", conversation_id).Update("status", "PROCESSING").Error

	return
}

//激活会话,将会话设置为处理中的状态
func ActiveConversation(conversation_id interface{}, user_id interface{}) (err error) {
	err = db.Model(&Conversation{}).
		Where("id = ?", conversation_id).
		Updates(map[string]interface{}{"user_id": user_id, "status": "PROCESSING"}).
		Error

	return
}

func CleanUnread(conversation_id interface{}, dest string) (err error) {
	err = db.Model(&Conversation{}).Where("id = ?", conversation_id).Update(dest, 0).Error

	return
}
