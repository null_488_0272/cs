package models

import (
	"errors"
	"time"
)

type Guest struct {
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	Uid       string    `json:"uid"`
	IP        string    `json:"ip"`
	City      string    `json:"city"`
	Vip       int       `json:"vip"`
	UpdatedAt time.Time `json:"updated_at"`
	CreatedAt time.Time `json:"created_at"`
}

func AddGuest(guest Guest) error {
	err := db.Create(&guest).Error

	return err
}

func MarkGuestAsVip(guest_id int) (err error) {
	var guest Guest
	err = db.Where("id = ?", guest_id).First(&guest).Error
	if err != nil {
		return
	}
	if guest.ID < 1 {
		err = errors.New("resource not found")
		return
	}
	guest.Vip ^= 1
	err = db.Save(&guest).Error

	return
}

func GetGuestByUid(uid string) (Guest, error) {
	var guset Guest
	err := db.Where("uid = ?", uid).First(&guset).Error

	return guset, err
}

func FindGuest(conditions map[string]interface{}) ([]Guest, error) {
	var gusets []Guest
	err := db.Where(conditions).Find(&gusets).Error

	return gusets, err
}
