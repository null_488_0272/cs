package models

type Tips struct {
	ID      int    `json:"id"`
	UserId  int    `json:"user_id"`
	Name    string `json:"name"`
	Content string `json:"content"`
}

func AddTips(tips Tips) (err error) {
	err = db.Create(tips).Error

	return
}

func DelTips(id int) (err error) {
	err = db.Where("id = ? ", id).Delete(Tips{}).Error

	return
}

func GetTipsByUid(uid int) (tipsList []Tips, err error) {
	err = db.Where("user_id = ? ", uid).Find(&tipsList).Error

	return
}
